import { TestBed } from '@angular/core/testing';

import { VerseLikeService } from './verse-like.service';

describe('VerseLikeService', () => {
  let service: VerseLikeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VerseLikeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
