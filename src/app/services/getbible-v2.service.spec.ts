import { TestBed } from '@angular/core/testing';

import { GetbibleV2Service } from './getbible-v2.service';

describe('GetbibleV2Service', () => {
  let service: GetbibleV2Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetbibleV2Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
