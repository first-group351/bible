import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Subject } from 'rxjs';
import { Plan } from '../models/plan.model';
import { AngularFireAuth } from '@angular/fire/compat/auth';

@Injectable({
  providedIn: 'root',
})
export class PlansService {
  private plansSubject = new Subject<{ plans: Plan[]; ids: string[] }>();
  plans$ = this.plansSubject.asObservable();
  private startedSubject = new Subject<[]>();
  startedPlans$ = this.startedSubject.asObservable();
  userId: string = '';
  userName: string | null = '';
  private initialized = false;

  constructor(
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth
  ) {
    this.afAuth.authState.subscribe((user) => {
      if (user) {
        this.userId = user.uid;
        this.userName = user?.displayName;
        this.initialize();
      } else {
        this.userId = '';
        this.userName = '';
        this.initialized = false;
      }
    });
  }

  private initialize(): void {
    if (this.initialized || !this.userId) {
      return;
    }

    this.getPlans();
    this.initialized = true;
  }

  savePlan(plan: Plan): Promise<void> {
    if (!this.userId) {
      return Promise.reject('User not authenticated');
    }

    const userPlansRef = this.firestore
      .collection('custom-plans')
      .doc(this.userId);

    return userPlansRef
      .get()
      .toPromise()
      .then((doc) => {
        if (doc?.exists) {
          const userData: any = doc.data();
          const existingPlans = userData.plans || [];
          const updatedPlans = [...existingPlans, plan];
          const existinIds = userData.ids || [];
          const updatedIds = [...existinIds, Date.now().toString()];
          return userPlansRef.set(
            {
              ids: updatedIds,
              userName: this.userName,
              plans: updatedPlans,
            },
            { merge: true }
          );
        } else {
          return userPlansRef.set(
            {
              ids: [Date.now().toString()],
              userName: this.userName,
              plans: [plan],
            },
            { merge: true }
          );
        }
      });
  }

  getPlans(): void {
    if (!this.userId) {
      this.plansSubject.next({ plans: [], ids: [] });
      return;
    }

    const userPlansRef = this.firestore
      .collection('custom-plans')
      .doc(this.userId);

    userPlansRef.get().subscribe((doc: any) => {
      if (doc.exists) {
        const userData = doc.data();
        const plans = userData.plans as Plan[];
        const ids = userData.ids as string[];
        this.plansSubject.next({ plans: plans, ids: ids });
      } else {
        this.plansSubject.next({ plans: [], ids: [] });
      }
    });
  }

  startPlan(id: string, day: number, finished: boolean): Promise<void> {
    if (!this.userId) {
      return Promise.reject('User not authenticated');
    }

    const userPlansRef = this.firestore
      .collection('started-plans')
      .doc(this.userId);

    return userPlansRef
      .get()
      .toPromise()
      .then((doc) => {
        if (doc?.exists) {
          const userData: any = doc.data();
          const startedPlans = userData.startedPlans || [];

          let updatedPlans;
          startedPlans.find((x: any) => x.planId === id)
            ? (updatedPlans = [{ planId: id, day: day, finished: finished }])
            : (updatedPlans = [
                ...startedPlans,
                { planId: id, day: day, finished: finished },
              ]);

          return userPlansRef.set(
            {
              startedPlans: updatedPlans,
            },
            { merge: true }
          );
        } else {
          return userPlansRef.set(
            { startedPlans: [{ planId: id, day: day, finished: finished }] },
            { merge: true }
          );
        }
      });
  }

  getStartedPlans(): void {
    if (!this.userId) {
      this.startedSubject.next([]);
      return;
    }

    const userPlansRef = this.firestore
      .collection('started-plans')
      .doc(this.userId);

    userPlansRef.get().subscribe((doc: any) => {
      if (doc.exists) {
        const userData = doc.data();
        this.startedSubject.next(userData);
      }
    });
  }
}
