import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ColorPickerModule } from 'ngx-color-picker';

import { IonicModule } from '@ionic/angular';

import { SettingsModalComponent } from './settings-modal.component';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, ColorPickerModule],
  declarations: [SettingsModalComponent],
  exports: [SettingsModalComponent],
})
export class SettingsModalComponentModule {}
