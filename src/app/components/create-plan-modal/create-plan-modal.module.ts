import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreatePlanModalComponent } from './create-plan-modal.component';
import { SelectPassageComponentModule } from '../select-passage/select-passage.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectPassageComponentModule,
  ],
  declarations: [CreatePlanModalComponent],
  exports: [CreatePlanModalComponent],
})
export class CreatePlanModalComponentModule {}
