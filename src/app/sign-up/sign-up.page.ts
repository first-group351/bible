import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage {
  email: string = '';
  password: string = '';
  username: string = '';

  constructor(private authService: AuthService) {}

  signUp() {
    this.authService.signUp(this.email, this.password, this.username);
  }
}
