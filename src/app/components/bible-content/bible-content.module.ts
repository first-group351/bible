import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BibleContentComponent } from './bible-content.component';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule],
  declarations: [BibleContentComponent],
  exports: [BibleContentComponent],
})
export class BibleContentComponentModule {}
