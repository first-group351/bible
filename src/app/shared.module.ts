import { NgModule } from '@angular/core';
import { SignInComponent } from './components/auth/signin/signin.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [SignInComponent],
  imports: [CommonModule, FormsModule, IonicModule],
  exports: [SignInComponent],
})
export class SharedModule {}
