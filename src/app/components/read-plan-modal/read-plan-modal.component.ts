import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { Plan } from 'src/app/models/plan.model';
import { Setting } from 'src/app/models/settings.model';
import { GetbibleV2Service } from 'src/app/services/getbible-v2.service';
import { UserSettingsService } from 'src/app/services/user-settings.service';

@Component({
  selector: 'app-read-plan-modal',
  templateUrl: './read-plan-modal.component.html',
  styleUrls: ['./read-plan-modal.component.scss'],
})
export class ReadPlanModalComponent implements OnInit, OnDestroy {
  @Input() plan: Plan | any;
  @Input() day: number = 0;
  bible: any;
  loaded: boolean = false;
  done: boolean = false;
  actualText: any;
  settings: Setting = {
    fontColor: '#000000',
    fontSize: 20,
    highlight: '#34ebcc',
  };
  settingsSubscription!: Subscription;
  multiple: boolean = false;
  doneDelay = false;
  index: number = 0;

  constructor(
    private modalController: ModalController,
    private getBible: GetbibleV2Service,
    private settingsService: UserSettingsService
  ) {}

  ngOnInit(): void {
    this.stepDelay();

    this.settingsSubscription = this.settingsService
      .getUserSettings()
      .subscribe((settings: Setting) => {
        if (settings) {
          this.settings = settings;
        }
      });

    this.getBible.getBible('kjv').subscribe(
      (res: any) => {
        this.bible = res;
        this.loaded = true;
        this.findTheText();
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

  ngOnDestroy(): void {
    this.settingsSubscription.unsubscribe();
  }

  stepDelay() {
    this.doneDelay = false;
    setTimeout(() => {
      this.doneDelay = true;
    }, 5000);
  }

  findTheText() {
    let dailyPlan = '';
    let book: any;
    let chapter: any;
    let verse: any;
    if (Array.isArray(this.plan.reading[this.day].chapter)) {
      dailyPlan = this.plan.reading[this.day].chapter[this.index];
      this.multiple = true;
    } else {
      dailyPlan = this.plan.reading[this.day].chapter;
    }

    book = this.bible.books.find((x: any) => dailyPlan.includes(x.name));
    chapter = book.chapters.find((x: any) => dailyPlan.includes(x.name));
    if (dailyPlan === chapter.name) {
      this.done = true;
    } else {
      verse = chapter.verses.find((x: any) => dailyPlan === x.name);
      this.done = true;
    }
    if (this.done)
      verse ? (this.actualText = verse) : (this.actualText = chapter);
  }

  finishPlan() {
    if (
      this.multiple &&
      this.index !== this.plan.reading[this.day].chapter.length - 1
    ) {
      this.index++;
      this.stepDelay();
      this.findTheText();
    } else {
      let finished = false;
      if (this.day + 1 === this.plan.reading.length) finished = true;

      this.day++;

      this.modalController.dismiss(
        { day: this.day, finished: finished },
        'confirm'
      );
    }
  }

  cancel() {
    return this.modalController.dismiss(null, 'cancel');
  }
}
