import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReadPlanModalComponent } from './read-plan-modal.component';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule],
  declarations: [ReadPlanModalComponent],
  exports: [ReadPlanModalComponent],
})
export class ReadPlanModalComponentModule {}
