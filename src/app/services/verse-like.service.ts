import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import firebase from 'firebase/compat/app';
import { BehaviorSubject, Observable } from 'rxjs';
import { LikedVerse } from '../models/likedVerse.model';

@Injectable({
  providedIn: 'root',
})
export class VerseLikeService {
  userId = '';
  private likesUpdated: BehaviorSubject<LikedVerse[]> = new BehaviorSubject<
    LikedVerse[]
  >([]);

  constructor(
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth
  ) {
    this.afAuth.authState.subscribe((user) => {
      if (user) {
        this.userId = user.uid;
        this.loadUserLikes();
      }
    });
  }

  private loadUserLikes(): void {
    this.firestore
      .collection('verse-likes')
      .doc(this.userId)
      .valueChanges()
      .subscribe((doc: any) => {
        if (doc) {
          this.likesUpdated.next(doc.likedVerses);
        }
      });
  }

  saveUserLikes(verseName: string, verseText: String): Promise<void> {
    const fieldValue = firebase.firestore.FieldValue;
    let verse = { verseName, verseText };
    if (this.userId) {
      const userLikesRef = this.firestore
        .collection('verse-likes')
        .doc(this.userId);
      return userLikesRef
        .get()
        .toPromise()
        .then((doc: any) => {
          if (doc.exists) {
            const likedVerses = doc.data().likedVerses || [];
            const existingVerse = likedVerses.find(
              (v: any) => v.verseName === verseName
            );

            if (existingVerse) {
              // Verse already liked, remove it
              return userLikesRef.update({
                likedVerses: fieldValue.arrayRemove(existingVerse),
              });
            } else {
              // Verse not liked, add it
              return userLikesRef.update({
                likedVerses: fieldValue.arrayUnion(verse),
              });
            }
          } else {
            // User document doesn't exist, create it and add the liked verse
            return userLikesRef.set({ likedVerses: [verse] });
          }
        });
    }
    return Promise.reject('User not authenticated');
  }

  getUserLikes(): Observable<any> {
    return this.likesUpdated.asObservable();
  }
}
