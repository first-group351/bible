import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { SelectPassageComponent } from '../components/select-passage/select-passage.component';
import { GetbibleV2Service } from '../services/getbible-v2.service';

@Component({
  selector: 'bible-tab1',
  templateUrl: 'bible.page.html',
  styleUrls: ['bible.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BiblePage implements OnInit, OnDestroy {
  bibleTranslation: string = 'cornilescu';
  loaded: boolean = false;
  bible: any;
  translations: any[] = [];
  selectedVerse: any;
  selectedChapter: any;
  selectedBook: any;

  bibleServiceSubscription!: Subscription;
  tranlationSubscription!: Subscription;

  constructor(
    private bibleService: GetbibleV2Service,
    private modalCtrl: ModalController,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.tranlationSubscription = this.bibleService
      .getBibleTranslations()
      .subscribe(
        (translations: any[]) => {
          this.translations = translations;
          this.loaded = true;
          this.getTranslation();
        },
        (error: any) => {
          console.log(error);
          this.getTranslation();
        }
      );
  }

  ngOnDestroy() {
    this.bibleServiceSubscription.unsubscribe;
    this.tranlationSubscription.unsubscribe;
  }

  handleChange(translation: any) {
    this.bibleTranslation = translation.detail.value;
    this.getTranslation();
  }

  getTranslation() {
    this.bibleServiceSubscription = this.bibleService
      .getBible(this.bibleTranslation)
      .subscribe(
        (res) => {
          this.bible = res;
          this.changeDetectorRef.markForCheck();
        },
        (error) => {
          console.log(error);
        }
      );
  }

  async openModal() {
    const modal = await this.modalCtrl.create({
      component: SelectPassageComponent,
      componentProps: {
        bible: this.bible,
      },
    });
    modal.present();

    const { data, role } = await modal.onWillDismiss();

    if (role === 'confirm') {
      this.selectedVerse = data.verse;
      this.selectedChapter = data.chapter
        ? data.chapter
        : data.book.chapters[0];
      this.selectedBook = data.book;
      localStorage.setItem(
        'lastRead',
        JSON.stringify([
          data.book,
          data.chapter ? data.chapter : data.book.chapters[0],
        ])
      );
      this.changeDetectorRef.markForCheck();
    }
  }
}
