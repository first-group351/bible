import { Injectable } from '@angular/core';
import { GoogleAuthProvider } from 'firebase/auth';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { NavController } from '@ionic/angular';
import { ToastService } from './toast.service';
import { interval } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
import firebase from 'firebase/compat';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  toastText: string = '';
  private tokenExpirationTimer: any;

  constructor(
    public afAuth: AngularFireAuth,
    private navCtrl: NavController,
    private toastService: ToastService,
    private router: Router
  ) {}

  initAuthStateListener() {
    this.afAuth.onAuthStateChanged((user) => {
      if (user) {
        this.startTokenExpirationTimer(user);
      } else {
        this.clearTokenExpirationTimer();
        const currentRoute = this.router.url;
        const publicRoutes = ['/tabs/bible', '/signup'];
        if (!publicRoutes.includes(currentRoute)) {
          this.navCtrl.navigateForward('/login');
        }
      }
    });
  }

  private startTokenExpirationTimer(user: firebase.User | null) {
    if (user) {
      user.getIdTokenResult().then((tokenResult) => {
        const expirationTime = tokenResult.expirationTime;
        const expiresIn =
          new Date(expirationTime).getTime() - new Date().getTime();

        this.tokenExpirationTimer = interval(expiresIn)
          .pipe(takeWhile(() => this.afAuth.currentUser !== null))
          .subscribe(() => {
            user.refreshToken;
          });
      });
    }
  }
  private clearTokenExpirationTimer() {
    if (this.tokenExpirationTimer) {
      this.tokenExpirationTimer.unsubscribe();
      this.tokenExpirationTimer = null;
    }
  }

  GoogleAuth() {
    return this.AuthLogin(new GoogleAuthProvider());
  }

  AuthLogin(provider: any) {
    return this.afAuth
      .signInWithPopup(provider)
      .then((result) => {
        this.toastText = 'Welcome ' + result.user?.displayName;
        this.toastService.showToast(this.toastText);
        this.navCtrl.navigateForward('/tabs/bible');
      })
      .catch((error) => {
        this.toastText = error.message;
        this.toastService.showToast(this.toastText);
      });
  }

  signUp(email: string, password: string, username: string) {
    this.afAuth
      .createUserWithEmailAndPassword(email, password)
      .then((data) => {
        let newUser = data.user;

        newUser
          ?.updateProfile({
            displayName: username,
            photoURL: '',
          })
          .then((res) => {
            console.log(res);
          })
          .catch((err) => {
            console.log(err);
          });

        this.toastText = 'Welcome ' + username;
        this.toastService.showToast(this.toastText);
        this.navCtrl.navigateForward('/tabs/bible');
      })
      .catch((error) => {
        console.log(error);
        this.toastText = error.message;
        this.toastService.showToast(this.toastText);
      });
  }

  login(email: string, password: string) {
    this.afAuth
      .signInWithEmailAndPassword(email, password)
      .then((data) => {
        this.toastText = 'Welcome ' + data.user?.displayName;
        this.toastService.showToast(this.toastText);
        this.navCtrl.navigateRoot('/tabs/bible');
      })
      .catch((error) => {
        this.toastText = error.message;
        this.toastService.showToast(this.toastText);
      });
  }

  logout() {
    this.afAuth.signOut().then(() => {
      this.toastService.showToast('Logged out successfully.');
      this.navCtrl.navigateForward('/login');
    });
  }
}
