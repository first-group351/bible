import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable, Subject } from 'rxjs';
import { Setting } from '../models/settings.model';
import { AngularFireAuth } from '@angular/fire/compat/auth';

@Injectable({
  providedIn: 'root',
})
export class UserSettingsService {
  userId = '';
  private settingsUpdated = new Subject<Setting>();

  constructor(
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth
  ) {
    this.afAuth.authState.subscribe((user) => {
      if (user) {
        this.userId = user.uid;
        this.retrieveUserSettings();
      } else {
        this.userId = '';
        let settings: Setting = {
          fontColor: '#000000',
          fontSize: 20,
          highlight: '#34ebcc',
        };
        this.settingsUpdated.next(settings);
      }
    });
  }

  private retrieveUserSettings(): void {
    if (this.userId) {
      this.firestore
        .collection('user-settings')
        .doc(this.userId)
        .valueChanges()
        .subscribe((settings: any) => {
          this.settingsUpdated.next(settings);
        });
    }
  }

  saveUserSettings(settings: Setting): Promise<void> {
    if (this.userId) {
      return this.firestore
        .collection('user-settings')
        .doc(this.userId)
        .set(settings);
    }
    return Promise.reject('User not authenticated');
  }

  getUserSettings(): Observable<Setting> {
    return this.settingsUpdated.asObservable();
  }
}
