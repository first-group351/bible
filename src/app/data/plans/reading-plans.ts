import { addDays } from 'date-fns';

const startday = new Date();

const plan1 = {
  plan: 'Romans Reading Plan',
  start_date: startday,
  end_date: addDays(startday, 15),
  reading: [
    {
      date: startday,
      chapter: 'Romans 1',
    },
    {
      date: addDays(startday, 1),
      chapter: 'Romans 2',
    },
    {
      date: addDays(startday, 2),
      chapter: 'Romans 3',
    },
    {
      date: addDays(startday, 3),
      chapter: 'Romans 4',
    },
    {
      date: addDays(startday, 4),
      chapter: 'Romans 5',
    },
    {
      date: addDays(startday, 5),
      chapter: 'Romans 6',
    },
    {
      date: addDays(startday, 6),
      chapter: 'Romans 7',
    },
    {
      date: addDays(startday, 7),
      chapter: 'Romans 8',
    },
    {
      date: addDays(startday, 8),
      chapter: 'Romans 9',
    },
    {
      date: addDays(startday, 9),
      chapter: 'Romans 10',
    },
    {
      date: addDays(startday, 10),
      chapter: 'Romans 11',
    },
    {
      date: addDays(startday, 11),
      chapter: 'Romans 12',
    },
    {
      date: addDays(startday, 12),
      chapter: 'Romans 13',
    },
    {
      date: addDays(startday, 13),
      chapter: 'Romans 14',
    },
    {
      date: addDays(startday, 14),
      chapter: 'Romans 15',
    },
    {
      date: addDays(startday, 15),
      chapter: 'Romans 16',
    },
  ],
};

const plan2 = {
  plan: 'Jonah Reading Plan',
  start_date: startday,
  end_date: addDays(startday, 3),
  reading: [
    {
      date: startday,
      chapter: 'Jonah 1',
    },
    {
      date: addDays(startday, 1),
      chapter: 'Jonah 2',
    },
    {
      date: addDays(startday, 2),
      chapter: 'Jonah 3',
    },
    {
      date: addDays(startday, 3),
      chapter: 'Jonah 4',
    },
  ],
};

export const ReadingPlans = [plan1, plan2];
