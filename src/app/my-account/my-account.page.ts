import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { AuthService } from '../services/auth.service';
import { UserSettingsService } from '../services/user-settings.service';
import { ToastService } from '../services/toast.service';
import { Setting } from '../models/settings.model';
import { Subscription } from 'rxjs';
import { ModalController } from '@ionic/angular';
import { SettingsModalComponent } from '../components/settings-modal/settings-modal.component';
import { VerseLikeService } from '../services/verse-like.service';
import { LikedVerse } from '../models/likedVerse.model';

@Component({
  selector: 'app-my-account',
  templateUrl: 'my-account.page.html',
  styleUrls: ['my-account.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MyAccountPage implements OnInit, OnDestroy {
  userId: string = '';
  settings: Setting = {
    fontSize: 20,
    fontColor: '#000000',
    highlight: '#34ebcc',
  };
  loaded = false;
  settingsSubscription!: Subscription;
  likeSubscription!: Subscription;
  likedVerses: LikedVerse[] = [];

  constructor(
    private userSettingsService: UserSettingsService,
    private verseLikeService: VerseLikeService,
    private authService: AuthService,
    private toast: ToastService,
    private modalController: ModalController,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.settingsSubscription = this.userSettingsService
      .getUserSettings()
      .subscribe((settings: Setting) => {
        if (settings) {
          this.settings = settings;
        }
        this.loaded = true;
      });

    this.likeSubscription = this.verseLikeService
      .getUserLikes()
      .subscribe((likes) => {
        this.likedVerses = likes;
        this.loaded = true;
        this.changeDetectorRef.markForCheck();
      });
  }

  ngOnDestroy(): void {
    this.settingsSubscription.unsubscribe();
  }

  calculateViewportHeight(): string {
    switch (this.likedVerses.length) {
      case 1:
        return '15vh';
      case 2:
        return '25vh';
      case 3:
        return '35vh';
      default:
        return '50vh';
    }
  }

  saveUserSettings() {
    this.userSettingsService
      .saveUserSettings(this.settings)
      .then(() => {
        this.toast.showToast('User settings saved successfully!', 2000);
        this.changeDetectorRef.markForCheck();
      })
      .catch((error) => {
        this.toast.showToast(error, 2000);
        this.changeDetectorRef.markForCheck();
      });
  }

  getHalfViewportHeight(): number {
    const viewportHeight = window.innerHeight;
    const halfViewportHeight = viewportHeight * 0.5;
    return halfViewportHeight;
  }

  async openSettingsModal() {
    const modal = await this.modalController.create({
      component: SettingsModalComponent,
      componentProps: {
        settings: this.settings,
      },
    });

    modal.onDidDismiss().then((result) => {
      if (result && result.data) {
        this.settings = result.data;
        this.saveUserSettings();
      }
    });

    return await modal.present();
  }

  logOut() {
    this.authService.logout();
  }
}
