// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAp5Y-Wrkdkgy_2lYlcO84f9wXaBNTMauE',
    authDomain: 'my-bible-8f92c.firebaseapp.com',
    projectId: 'my-bible-8f92c',
    storageBucket: 'my-bible-8f92c.appspot.com',
    messagingSenderId: '359217925231',
    appId: '1:359217925231:web:02531990561afcacac919a',
    measurementId: 'G-JXGB1Q0NZ1',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
