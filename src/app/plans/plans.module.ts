import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PlansPage } from './plans.page';
import { PlansPageRoutingModule } from './plans-routing.module';
import { PlanComponentModule } from '../components/plan/plan.module';
import { CreatePlanModalComponent } from '../components/create-plan-modal/create-plan-modal.component';
import { CreatePlanModalComponentModule } from '../components/create-plan-modal/create-plan-modal.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    PlansPageRoutingModule,
    PlanComponentModule,
    CreatePlanModalComponentModule,
  ],
  declarations: [PlansPage],
  entryComponents: [CreatePlanModalComponent],
})
export class PlansPageModule {}
