import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { addDays } from 'date-fns';
import { GetbibleV2Service } from 'src/app/services/getbible-v2.service';
import { SelectPassageComponent } from '../select-passage/select-passage.component';

@Component({
  selector: 'app-create-plan-modal',
  templateUrl: './create-plan-modal.component.html',
  styleUrls: ['./create-plan-modal.component.scss'],
})
export class CreatePlanModalComponent implements OnInit {
  plan: string = '';
  start_date = new Date();
  days?: number;
  end_date = new Date(addDays(this.start_date, 1));
  today = new Date().toISOString();
  reading: { date: Date; chapter: [] }[] = [];
  bible: any;
  planData: any;

  constructor(
    private modalController: ModalController,
    private getBible: GetbibleV2Service
  ) {}

  ngOnInit(): void {
    this.getBible.getBible('kjv').subscribe(
      (res: any) => {
        this.bible = res;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

  cancel() {
    return this.modalController.dismiss(null, 'cancel');
  }

  savePlan() {
    const planData = {
      plan: this.plan,
      start_date: this.start_date,
      end_date: new Date(addDays(this.start_date, this.days ? this.days : 0)),
      reading: this.reading,
    };

    this.modalController.dismiss(planData, 'confirm');
  }

  changeDate(value: any) {
    this.start_date = new Date(value);
  }

  async openModal(index: number) {
    const modal = await this.modalController.create({
      component: SelectPassageComponent,
      componentProps: {
        bible: this.bible,
        plan: true,
      },
    });
    modal.present();

    const { data, role } = await modal.onWillDismiss();

    if (role === 'confirm') {
      this.planData = data;
      let chapters: any = [];
      data.forEach((element: any) => {
        chapters.push(element?.name);
      });
      this.reading[index] = {
        date: new Date(addDays(this.start_date, index - 1)),
        chapter: chapters,
      };
    }
  }
}
