import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'bible',
        loadChildren: () =>
          import('../bible/bible.module').then((m) => m.BiblePageModule),
      },
      {
        path: 'my-account',
        loadChildren: () =>
          import('../my-account/my-account.module').then(
            (m) => m.MyAccountPageModule
          ),
      },
      {
        path: 'plans',
        loadChildren: () =>
          import('../plans/plans.module').then((m) => m.PlansPageModule),
      },
      {
        path: '',
        redirectTo: 'bible',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '',
    redirectTo: 'tabs',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
