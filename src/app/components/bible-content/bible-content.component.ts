import {
  Component,
  Input,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  OnChanges,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { LikedVerse } from 'src/app/models/likedVerse.model';
import { Setting } from 'src/app/models/settings.model';
import { UserSettingsService } from 'src/app/services/user-settings.service';
import { VerseLikeService } from 'src/app/services/verse-like.service';

@Component({
  selector: 'app-bible-content',
  templateUrl: './bible-content.component.html',
  styleUrls: ['./bible-content.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BibleContentComponent implements OnInit, OnChanges, OnDestroy {
  @Input() bible: any;
  @Input() selectedVerse: any[] = [];
  @Input() selectedChapter: any;
  @Input() selectedBook: any;
  settings: Setting = {
    fontColor: '#000000',
    fontSize: 20,
    highlight: '#34ebcc',
  };
  settingsSubscription!: Subscription;
  likeSubscription!: Subscription;
  userLikes: LikedVerse[] = [];
  likeNames: String[] = [];
  loaded: boolean = false;

  constructor(
    private settingsService: UserSettingsService,
    private verseLikeService: VerseLikeService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.getFromStorage();
    this.settingsSubscription = this.settingsService
      .getUserSettings()
      .subscribe((settings: Setting) => {
        if (settings) {
          this.settings = settings;
        }
        this.changeDetectorRef.markForCheck();
      });

    this.likeSubscription = this.verseLikeService
      .getUserLikes()
      .subscribe((likes) => {
        this.likeNames = [];
        this.userLikes = likes;
        this.userLikes.forEach((like) => {
          this.likeNames.push(like.verseName);
        });
        this.loaded = true;
        this.changeDetectorRef.markForCheck();
      });
  }

  ngOnChanges() {
    this.updateOnChangeTranslation();
    this.changeDetectorRef.markForCheck();
  }

  ngOnDestroy(): void {
    this.settingsSubscription.unsubscribe();
    this.likeSubscription.unsubscribe();
  }

  saveOnStorage() {
    localStorage.setItem(
      'lastRead',
      JSON.stringify([this.selectedBook, this.selectedChapter])
    );
  }

  handleMouseDown(event: MouseEvent): void {
    if (event.detail > 1) {
      event.preventDefault();
    }
  }

  handleDoubleClick(
    event: MouseEvent,
    verseName: string,
    verseText: string
  ): void {
    console.log(verseName);
    this.verseLikeService.saveUserLikes(verseName, verseText);
  }

  getFromStorage() {
    if (localStorage.getItem('lastRead')) {
      [this.selectedBook, this.selectedChapter] = JSON.parse(
        localStorage.getItem('lastRead') || '[]'
      );
    } else {
      this.selectedBook = this.bible.books[42];
      this.selectedChapter = this.bible.books[42].chapters[0];
    }
    this.changeDetectorRef.markForCheck();
  }

  updateOnChangeTranslation() {
    if (this.selectedBook) {
      this.selectedBook = this.bible.books[this.selectedBook.nr - 1];
    }
    if (this.selectedChapter) {
      this.selectedChapter =
        this.bible.books[this.selectedBook.nr - 1].chapters[
          this.selectedChapter.chapter - 1
        ];
      this.saveOnStorage();
    }
    // if (this.selectedVerse) {
    //   this.selectedVerse =
    //     this.bible.books[this.selectedBook.nr - 1].chapters[
    //       this.selectedChapter.chapter - 1
    //     ].verses[this.selectedVerse.verse - 1];
    // }
  }

  back() {
    if (this.selectedChapter.chapter == 1) {
      this.selectedBook = this.bible.books[this.selectedBook.nr - 2];
      this.selectedChapter =
        this.selectedBook.chapters[this.selectedBook.chapters.length - 1];
    } else {
      this.selectedChapter =
        this.selectedBook.chapters[this.selectedChapter.chapter - 2];
    }
    this.saveOnStorage();
  }

  forward() {
    if (this.selectedChapter.chapter == this.selectedBook.chapters.length) {
      this.selectedBook = this.bible.books[this.selectedBook.nr];
      this.selectedChapter = this.selectedBook.chapters[0];
    } else {
      this.selectedChapter =
        this.selectedBook.chapters[this.selectedChapter.chapter];
    }
    this.saveOnStorage();
  }

  fullChapter() {
    this.selectedVerse = [];
  }
}
