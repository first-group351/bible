export interface Plan {
  plan: string;
  start_date: Date;
  end_date: Date;
  reading: {
    date: Date;
    chapter: string | [];
  }[];
}
