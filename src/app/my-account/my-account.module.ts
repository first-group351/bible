import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { ScrollingModule as ExperimentalScrollingModule } from '@angular/cdk-experimental/scrolling';
import { MyAccountPage } from './my-account.page';

import { MyAccountPageRoutingModule } from './my-account-routing.module';
import { SettingsModalComponentModule } from '../components/settings-modal/settings-modal.comonen.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    MyAccountPageRoutingModule,
    SettingsModalComponentModule,
    ScrollingModule,
    ExperimentalScrollingModule,
  ],
  declarations: [MyAccountPage],
})
export class MyAccountPageModule {}
