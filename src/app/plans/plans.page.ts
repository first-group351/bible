import { Component, OnInit } from '@angular/core';
import { ReadingPlans } from '../data/plans/reading-plans';
import { Plan } from '../models/plan.model';
import { ModalController } from '@ionic/angular';
import { CreatePlanModalComponent } from '../components/create-plan-modal/create-plan-modal.component';
import { PlansService } from '../services/plans.service';
import { ToastService } from '../services/toast.service';

@Component({
  selector: 'app-plans',
  templateUrl: 'plans.page.html',
  styleUrls: ['plans.page.scss'],
})
export class PlansPage implements OnInit {
  savedPlans: Plan[] = [];
  planIds: string[] = [];
  plansLoaded: boolean = false;

  constructor(
    private modalController: ModalController,
    private plansService: PlansService,
    private toast: ToastService
  ) {}

  ngOnInit(): void {
    this.addPlans();
  }

  addPlans(): void {
    // ReadingPlans.forEach((plan) => {
    //   this.savedPlans.push(plan);
    // });

    this.plansService.getPlans();

    this.plansService.plans$.subscribe((data) => {
      if (this.plansLoaded) {
        this.savedPlans.push(data.plans.slice(-1)[0]);
        this.planIds.push(data.ids.slice(-1)[0]);
      } else {
        data.plans.forEach((plan: any) => {
          this.savedPlans.push(plan);
        });
        data.ids.forEach((id: any) => {
          this.planIds.push(id);
        });

        this.plansLoaded = true;
      }
    });
  }

  savePlanOnFirestore(plan: Plan) {
    this.plansService
      .savePlan(plan)
      .then(() => {
        this.toast.showToast('Plan saved successfully', 2000);
        this.plansService.getPlans();
      })
      .catch((error) => {
        this.toast.showToast(`Failed to save plan: ${error}`, 2000);
      });
  }

  async openPlanModal() {
    const modal = await this.modalController.create({
      component: CreatePlanModalComponent,
    });

    modal.onDidDismiss().then((result) => {
      if (result.role === 'backdrop' || result.role === 'cancel') {
        console.log('cancel');
      } else if (result.role === 'confirm') {
        this.savePlanOnFirestore(result.data);
      }
    });

    return await modal.present();
  }
}
