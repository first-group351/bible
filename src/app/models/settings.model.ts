export interface Setting {
  fontSize: number;
  fontColor: string;
  highlight: string;
}
