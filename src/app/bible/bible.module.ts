import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BiblePage } from './bible.page';
import { MatTooltipModule } from '@angular/material/tooltip';

import { BiblePageRoutingModule } from './bible-routing.module';
import { BibleContentComponentModule } from '../components/bible-content/bible-content.module';
import { SelectPassageComponentModule } from '../components/select-passage/select-passage.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    BiblePageRoutingModule,
    BibleContentComponentModule,
    SelectPassageComponentModule,
    MatTooltipModule,
  ],
  declarations: [BiblePage],
})
export class BiblePageModule {}
