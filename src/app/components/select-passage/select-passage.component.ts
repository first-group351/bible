import { Component, Input } from '@angular/core';

import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-select-passage',
  templateUrl: './select-passage.component.html',
  styleUrls: ['./select-passage.component.scss'],
})
export class SelectPassageComponent {
  @Input() bible: any;
  @Input() plan: boolean = false;
  selectedBook: any;
  selected = false;
  selectedChapter: any;
  selectedChapterFlag = false;
  selectedVerse: any[] = [];
  planContent: any[] = [];
  title: string = 'Choose a book';

  constructor(private modalCtrl: ModalController) {}

  cancel() {
    return this.modalCtrl.dismiss(null, 'cancel');
  }

  confirm() {
    return this.modalCtrl.dismiss(
      !this.plan
        ? {
            book: this.selectedBook,
            chapter: this.selectedChapter,
            verse: this.selectedVerse,
          }
        : this.planContent,
      'confirm'
    );
  }

  selectBook(book: any) {
    if (this.selectedBook == book) {
      this.selectedBook = undefined;
      this.selected = false;
      this.title = `Choose a book`;
    } else {
      this.selectedBook = book;
      this.selected = true;
      this.title = `Chapter from ${book.name}`;
    }
  }

  selectChapter(chapter: any) {
    if (this.plan) {
      if (this.planContent.includes(chapter)) {
        const index = this.planContent.indexOf(chapter);
        this.planContent.splice(index, 1);
      } else {
        this.planContent.push(chapter);
      }
    } else {
      if (this.selectedChapter == chapter) {
        this.selectedChapter = undefined;
        this.selectedChapterFlag = false;
        this.title = `Chapter from ${chapter.name.substring(
          0,
          chapter.name.indexOf(':')
        )}`;
      } else {
        this.selectedChapter = chapter;
        this.selectedChapterFlag = true;
        this.title = `Verse from ${chapter.name}`;
      }
    }
  }

  selectVerse(verse: any) {
    if (this.plan) {
      if (this.planContent.includes(verse)) {
        const index = this.planContent.indexOf(verse);
        this.planContent.splice(index, 1);
      } else {
        this.planContent.push(verse);
      }
    } else {
      if (this.selectedVerse.includes(verse)) {
        const index = this.selectedVerse.indexOf(verse);
        this.selectedVerse.splice(index, 1);
      } else {
        this.selectedVerse.push(verse);
      }
      this.selectedVerse.sort((a, b) => a.verse - b.verse);
    }
  }

  toVerses(chapter: any) {
    if (this.selectedChapter == chapter) {
      this.selectedChapter = undefined;
      this.selectedChapterFlag = false;
      this.title = `Chapter from ${chapter.name.substring(
        0,
        chapter.name.indexOf(' ')
      )}`;
    } else {
      this.selectedChapter = chapter;
      this.selectedChapterFlag = true;
      this.title = `Verse from ${chapter.name}`;
    }
  }

  deselectV() {
    this.selectedChapterFlag = false;
    this.selectedVerse = [];
    this.title = `Chapter from ${this.selectedChapter.name.substring(
      0,
      this.selectedChapter.name.indexOf(' ')
    )}`;
  }

  deselectC() {
    this.selected = false;
    this.title = 'Choose a book';
  }
}
