import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { IonicModule } from '@ionic/angular';

import { PlanComponent } from './plan.component';
import { TimestampToDatePipe } from '../../pipes/timestamp-to-date';
import { ReadPlanModalComponentModule } from '../read-plan-modal/read-plan-modal.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ScrollingModule,
    ReadPlanModalComponentModule,
  ],
  declarations: [PlanComponent, TimestampToDatePipe],
  exports: [PlanComponent],
})
export class PlanComponentModule {}
