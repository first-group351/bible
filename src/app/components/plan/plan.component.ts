import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Plan } from 'src/app/models/plan.model';
import { ReadPlanModalComponent } from '../read-plan-modal/read-plan-modal.component';
import { PlansService } from 'src/app/services/plans.service';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.scss'],
})
export class PlanComponent implements OnInit {
  @Input() plan: Plan | any;
  @Input() planId: string = '';
  accordionExpanded = false;
  started = false;
  day: number = 0;
  finished = false;

  constructor(
    private modalController: ModalController,
    private plansService: PlansService
  ) {}

  ngOnInit() {
    this.plansService.getStartedPlans();

    this.plansService.startedPlans$.subscribe((data: any) => {
      let plan = data.startedPlans.find((x: any) => x.planId === this.planId);
      if (plan) {
        if (!this.started) {
          this.started = true;
          this.day = plan.day;
          this.finished = plan.finished;
        }
      }
    });
  }

  async openModal() {
    const modal = await this.modalController.create({
      component: ReadPlanModalComponent,
      componentProps: {
        plan: this.plan,
        day: this.day,
      },
    });
    modal.present();

    const { data, role } = await modal.onWillDismiss();

    if (role === 'confirm') {
      if (data.finished) this.finished = true;

      this.day = data.day;
      this.started = true;

      this.plansService.startPlan(this.planId, data.day, data.finished);

      this.plansService.getStartedPlans();
    }
  }

  toggleAccordion() {
    this.accordionExpanded = !this.accordionExpanded;
  }
}
