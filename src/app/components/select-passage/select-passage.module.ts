import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectPassageComponent } from '../select-passage/select-passage.component';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule],
  declarations: [SelectPassageComponent],
  exports: [SelectPassageComponent],
})
export class SelectPassageComponentModule {}
