import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Setting } from '../../models/settings.model';

@Component({
  selector: 'app-settings-modal',
  templateUrl: './settings-modal.component.html',
  styleUrls: ['./settings-modal.component.scss'],
})
export class SettingsModalComponent {
  @Input() settings!: Setting;
  @ViewChild('colorPicker1') colorPicker1!: ElementRef<HTMLInputElement>;
  @ViewChild('colorPicker2') colorPicker2!: ElementRef<HTMLInputElement>;

  constructor(private modalController: ModalController) {}

  openColorPickerFont() {
    this.colorPicker1.nativeElement.click();
  }

  openColorPickerHighlight() {
    this.colorPicker2.nativeElement.click();
  }

  dismissModal() {
    this.modalController.dismiss();
  }

  saveSettings() {
    this.modalController.dismiss(this.settings);
  }
}
