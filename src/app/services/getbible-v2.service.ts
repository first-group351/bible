import { Injectable } from '@angular/core';
import { catchError, from, Observable, of, tap } from 'rxjs';
import axios from 'axios';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class GetbibleV2Service {
  private baseURL: string = 'https://getbible.net/v2';

  constructor(private http: HttpClient) {}

  getBibleTranslations(): Observable<any[]> {
    return from(
      axios
        .get(`${this.baseURL}/translations.json`)
        .then((response) => {
          const translations = [
            response.data.akjv,
            response.data.karoli,
            response.data.cornilescu,
          ];
          return translations;
        })
        .catch((error) => {
          console.log(error);
          return [];
        })
    );
  }

  getBible(translation: string) {
    return this.http
      .get(`${this.baseURL}/${translation}.json`)
      .pipe(tap(), catchError(this.handleError<any[]>(`Get Bible`)));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
