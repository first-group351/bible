import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { NavController } from '@ionic/angular';
import { ToastService } from '../services/toast.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss'],
})
export class TabsPage {
  publicContentEnabled = false;

  constructor(
    private afAuth: AngularFireAuth,
    private navCtrl: NavController,
    private toast: ToastService
  ) {
    this.checkAuthStatus();
  }

  private checkAuthStatus() {
    this.afAuth.onAuthStateChanged((user) => {
      if (user) {
        // User is logged in
        this.publicContentEnabled = true;
      } else {
        // User is logged out
        this.publicContentEnabled = false;
      }
    });
  }

  login() {
    this.toast.showToast('You have to log in', 2000);
    this.navCtrl.navigateForward('login');
  }
}
